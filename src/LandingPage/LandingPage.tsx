import React, { useState } from "react";
import { Box, Modal, Typography } from "@mui/material";
import styled, { keyframes } from "styled-components";
import { fadeInUp } from "react-animations";
import "../assets/sass_component/_landing-page.sass";
import Header from "../components/Header/HeaderPart";

const AnimateDiv = styled.div`
  animation: 1.5s ${keyframes`${fadeInUp}`};
`;

const LandingPage = () => {
  return (
    <Box>
      {/* top bar sign up msg */}
      <AnimateDiv>
        <div className="discountmsg">hellos lorem epsum</div>
      </AnimateDiv>

      {/* main home content start */}
      <Box className="box-content-landing">
        {/* Header part */}
        <Header />
        <Typography id="modal-modal-description" variant="h4">
          Experience the new standard in travel with our hand-picked collection
          of sustainable luxury hole sandresorts.
        </Typography>
        <img className="logo-light" src="/images/icon-light.svg" alt="logo" />
      </Box>
      <Box className="filer-part">hello</Box>
    </Box>
  );
};

export default LandingPage;
