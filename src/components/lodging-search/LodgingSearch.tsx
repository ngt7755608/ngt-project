import { Box, ButtonBase, Divider } from "@mui/material";
import React from "react";
import "../../assets/sass_component/_lodging-search.sass";
import ModalForLodgeSearch from "./ModalForLodgeSearch";

interface LodgingSearchPropTypes {
  setModalOpening: Function;
}

const CustomDivider = () => {
  return (
    <Divider
      sx={{ margin: "0 10px 0 10px", color: "black" }}
      orientation="vertical"
      variant="middle"
      flexItem
    />
  );
};

const LodgingSearch: React.FC<LodgingSearchPropTypes> = ({
  setModalOpening,
}) => {
  return (
    <Box className="box-content">
      <div className="main-content">
        <img src="/images/icon-dark.svg" alt="logo" />
        <Box className="search-text">
          <div
            className="text-clickable"
            onClick={() => {
              setModalOpening(true);
            }}
          >
            Where are you headed ?
          </div>
          <CustomDivider />
          <div className="text-clickable"> When are you going ?</div>
          <CustomDivider />
          <div className="text-clickable">Who you are going with ?</div>
        </Box>
      </div>
    </Box>
  );
};

export default LodgingSearch;
