import React from "react";
import {
  Autocomplete,
  Box,
  IconButton,
  InputAdornment,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import "../../assets/sass_component/_modal-lodge-search.sass";
import LanguageIcon from "@mui/icons-material/Language";
import AccessTimeIcon from "@mui/icons-material/AccessTime";

const closeIcon = {
  fontSize: "3rem",
};

const top100Films = [
  { title: "The Shawshank Redemption", year: 1994 },
  { title: "The Godfather", year: 1972 },
  { title: "The Godfather: Part II", year: 1974 },
  { title: "The Dark Knight", year: 2008 },
  { title: "12 Angry Men", year: 1957 },
  { title: "Schindler's List", year: 1993 },
  { title: "Pulp Fiction", year: 1994 },
];
const recentSearches = [
  { name: "Kothrud, Pune" },
  { name: "Opp Kashi Bhavan, Bhayander (east), Mumbai" },
  { name: "Kempe Gowda Nagar, Banglore" },
];

interface ModalForLodgeSearchPropTypes {
  onModalClose: Function;
}

const ModalForLodgeSearch: React.FC<ModalForLodgeSearchPropTypes> = ({
  onModalClose,
}) => {
  return (
    <Modal
      open={true}
      // onClose={() => {
      //   onModalClose(false);
      // }}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="modal-box-content">
        <div className="first-label">
          <Typography
            id="modal-modal-description"
            variant="h5"
            sx={{ fontWeight: "bold" }}
          >
            Where are you headed?
          </Typography>
          <IconButton
            onClick={() => {
              onModalClose(false);
            }}
            aria-label="delete"
            color="#fff"
          >
            <CloseIcon sx={closeIcon} />
          </IconButton>
        </div>
        <div className="input-search-field">
          <Autocomplete
            id="free-solo-demo"
            freeSolo
            options={top100Films.map((option) => option.title)}
            renderInput={(params) => (
              <TextField
                {...params}
                placeholder="Enter a Destination..."
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LanguageIcon color="orange" />
                    </InputAdornment>
                  ),
                }}
              />
            )}
          />
        </div>

        {/* recent search */}
        <div>
          <Typography
            id="modal-modal-description"
            variant="h6"
            sx={{ fontWeight: 1 && "bold" }}
          >
            Recent Searches
          </Typography>
          <Box className="recent-search-result">
            {recentSearches.map((res) => (
              <div className="recent-search-content">
                <AccessTimeIcon />
                <Typography>{res.name}</Typography>
              </div>
            ))}
            {/* {recentSearches.map((res) => (
              <div className="recent-search-content">
                <AccessTimeIcon />
                <Typography>{res.name}</Typography>
              </div>
            ))} */}
          </Box>
        </div>

        {/* button */}
        <div className="next-btn">
          <button type="button" className="btn">
            NEXT
          </button>
        </div>
      </Box>
    </Modal>
  );
};

export default ModalForLodgeSearch;
