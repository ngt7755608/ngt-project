import React, { useState } from "react";
import { IconButton } from "@mui/material";
import { Box } from "@mui/system";
import LodgingSearch from "../lodging-search/LodgingSearch";
import MenuIcon from "@mui/icons-material/Menu";
import "../../assets/sass_component/_header-part.sass";
import ModalForLodgeSearch from "../lodging-search/ModalForLodgeSearch";

const Header = () => {
  const [openModalForLodgingSearch, setOpenModalForLodgingSearch] =
    useState<boolean>(false);

  return (
    <div className="home-content">
      {/* svg icon NGT */}
      <img src="/images/text-ngt-light.svg" alt="No Guilt Trip" />

      {/* search lodge */}
      <LodgingSearch setModalOpening={setOpenModalForLodgingSearch} />

      {/* menu bar */}
      <div className="menu-bar">
        <IconButton
          aria-label="delete"
          color="#fff"
          className="header-menu-bar"
          sx={{
            "&:hover": {
              backgroundColor: "lightgrey",
            },
          }}
        >
          <MenuIcon />
        </IconButton>
      </div>

      {openModalForLodgingSearch ? (
        <ModalForLodgeSearch onModalClose={setOpenModalForLodgingSearch} />
      ) : null}
    </div>
  );
};

export default Header;
